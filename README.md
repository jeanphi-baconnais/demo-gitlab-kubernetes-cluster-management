### Cluster management template project

Example [cluster management](https://docs.gitlab.com/ee/user/clusters/management_project_template.html) project.

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

For more information, see [the documentation for this template](https://docs.gitlab.com/ee/user/clusters/management_project_template.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/cluster-management).


# Notes 

- Création Cluster Kubernetes “demo-gitlab-kubernetes-cluster-managemen” par GKE : 
    - 3 noeuds
    - + de CPU (noeuds > e2-standard-2)
    - automatisation > activer autoscaling vertifcal
- Infrastructure > Kubernetes > ajouter l’agent
- Créer l’agent sur le cluster à l’aide de la commande générée
- Modifier l’url de Prometheus Monitor > Metrics > modify
